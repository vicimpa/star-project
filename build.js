const child_process_1 = require("child_process");

function runCommand(command, cwd = process.cwd()) {
    let child = child_process_1.exec(command, {
        cwd: cwd
    });
    child.stdout.on('data', d => process.stdout.write(d.toString()));
    child.stderr.on('data', d => process.stderr.write(d.toString()));
}

runCommand('tsc', require('path').join(__dirname, 'scripts'));
runCommand('webpack --config webpack.config.js --watch');
runCommand('sass --sourcemap=none --watch styles:styles --style compressed');