import * as THREE from 'three';

export class ViewScene{
    private scene: THREE.Scene = new THREE.Scene();
    private camera: THREE.PerspectiveCamera = new THREE.PerspectiveCamera();
    private renderer: THREE.WebGLRenderer = new THREE.WebGLRenderer();

    private _width: number = 0;
    private _height: number = 0;

    private work: boolean = false;

    public domElement: HTMLCanvasElement = this.renderer.domElement;

    public cameraPosition = this.camera.position;
    public cameraRotation = this.camera.rotation;

    constructor(runner: boolean = false, appends: HTMLElement = null){
        if(runner)
            this.run();

        if(appends)
            appends.appendChild(this.domElement);
    }

    public run(){
        this.work = true;
        this.tick();
    }

    public stop(){
        this.work = false;
    }

    private tick(){
        if(window.innerWidth !== this._width || window.innerHeight !== this._height){
            this._width = window.innerWidth;
            this._height = window.innerHeight;

            this.camera.fov = 45;
            this.camera.near = 1;
            this.camera.far = 3000;
            this.camera.aspect = this._width / this._height;
            this.renderer.setSize(this._width, this._height);

            this.camera.updateProjectionMatrix();

        }

        if(this.work)
            requestAnimationFrame( () => this.tick() );

        this.renderer.render(this.scene, this.camera);
    }

    public add(mesh: any){
        this.scene.add(mesh);
    }
}