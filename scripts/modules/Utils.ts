export interface Position{
    x: number
    y: number
}

export const CyrclePosition = (radius: number, angle: number, center: Position = {x: 0, y: 0}): Position => {
    return {
        x: parseFloat((center.x + Math.cos(angle) * radius).toFixed(2)),
        y: parseFloat((center.y + Math.sin(angle) * radius).toFixed(2))
    }
};

export const CyrcleAnimation = (radius: number, angle: number, speed: number = 25, center: Position, callback: (pos: Position) => void) => {
    let lastPosition: Position = null;
    let startAngle = angle;

    function animate(){
        let nowPosition = CyrclePosition(radius, angle, center);

        callback(nowPosition);

        if(!lastPosition)
            lastPosition = nowPosition;

        else if(lastPosition.x === nowPosition.x && lastPosition.y === nowPosition.y){
            angle = startAngle;
            lastPosition.x = nowPosition.x;
            lastPosition.y = nowPosition.y;
        }
        angle += speed / 10000;

        setTimeout(animate, 5);
    }

    animate();
};