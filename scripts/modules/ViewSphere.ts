import * as THREE from 'three';

import {ViewScene} from "./ViewScene";

export class ViewSphere{
    public geometry: THREE.Geometry;
    public material: THREE.Material;

    public mesh: THREE.Mesh;

    constructor(radius: number, color: number = 0xffffff, create: boolean = true){
        this.geometry = <THREE.Geometry>(new THREE.SphereGeometry(radius, radius * 4, radius * 4));
        this.material = new THREE.MeshPhysicalMaterial({color: color});

        if(create)
            this.create();
    }

    public create(){
        this.mesh = new THREE.Mesh(this.geometry, this.material);
    }

    public get objectPosition(){
        return this.mesh.position;
    }

    public get objectRotation(){
        return this.mesh.rotation;
    }

    public append(scene: ViewScene){
        scene.add(this.mesh);
    }
}