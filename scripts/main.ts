import * as THREE from 'three';
import {ViewScene} from "./modules/ViewScene";
import {ViewSphere} from "./modules/ViewSphere";
import * as utils from './modules/Utils';

let scene = new ViewScene(true, document.body);

let sun = new ViewSphere(30, 0xffff00, false);
sun.material = new THREE.MeshBasicMaterial({color: 0xffff33});
sun.create();

let light = new THREE.PointLight(0xffffff, 8, 0, 4);

let merc = new ViewSphere(3, 0xAAAA55);
let vener = new ViewSphere(5, 0x998833);
let zeml = new ViewSphere(10, 0x2233ff);
let moon = new ViewSphere(2, 0xaaaaaa);
let mars = new ViewSphere(7, 0xff0000);
let yup = new ViewSphere(16, 0x336611);
let sat = new ViewSphere(14, 0xd3a55d);
let satCil = new THREE.Mesh(new THREE.CircleBufferGeometry( 20, 60 ), new THREE.MeshBasicMaterial({color: 0xd3a55d}));

scene.add(light);
scene.add(satCil);
sun.append(scene);
merc.append(scene);
vener.append(scene);
zeml.append(scene);
moon.append(scene);
mars.append(scene);
yup.append(scene);
sat.append(scene);

light.position.x = 0;
light.position.y = 1;
light.position.z = 0;


scene.cameraPosition.y = -600;
scene.cameraPosition.z = 800;

scene.cameraRotation.x = 0.6;

utils.CyrcleAnimation(50, 0, 28, {x: 0, y: 0}, (info: utils.Position) => {
   merc.objectPosition.x = info.x;
   merc.objectPosition.y = info.y;
});

utils.CyrcleAnimation(130, 3, 20, {x: 0, y: 0}, (info: utils.Position) => {
    vener.objectPosition.x = info.x;
    vener.objectPosition.y = info.y;
});

utils.CyrcleAnimation(190, 2, 16, {x: 0, y: 0}, (info: utils.Position) => {
    zeml.objectPosition.x = info.x;
    zeml.objectPosition.y = info.y;
});

utils.CyrcleAnimation(20, 1, -70, zeml.objectPosition, (info: utils.Position) => {
    moon.objectPosition.x = info.x;
    moon.objectPosition.y = info.y;
});

utils.CyrcleAnimation(260, -1, 12, {x: 0, y: 0}, (info: utils.Position) => {
    mars.objectPosition.x = info.x;
    mars.objectPosition.y = info.y;
});

utils.CyrcleAnimation(320, -1.4, 6, {x: 0, y: 0}, (info: utils.Position) => {
    yup.objectPosition.x = info.x;
    yup.objectPosition.y = info.y;
});
utils.CyrcleAnimation(440, -0.2, 3, {x: 0, y: 0}, (info: utils.Position) => {
    sat.objectPosition.x = info.x;
    sat.objectPosition.y = info.y;
    satCil.position.x = info.x;
    satCil.position.y = info.y;
});