const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: [path.join(__dirname, 'tmp/main.js')],
    output: {
        path: path.join(__dirname, 'scripts'),
        filename: 'main.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            }
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false }
        })
    ]
};